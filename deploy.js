import chromeWebstoreUpload from 'chrome-webstore-upload';
import fs from 'fs';
import zipFolder from 'zip-folder';
let folder = './';
let zipName = 'extension.zip';

// credentials and IDs from gitlab-ci.yml file (your appropriate config file)
let REFRESH_TOKEN = process.env.REFRESH_TOKEN; 
let EXTENSION_ID = process.env.EXTENSION_ID;
let CLIENT_SECRET = process.env.CLIENT_SECRET;
let CLIENT_ID = process.env.CLIENT_ID;

const store = chromeWebstoreUpload({
  extensionId: EXTENSION_ID,
  clientId: CLIENT_ID,
  refreshToken:REFRESH_TOKEN,
  clientSecret: CLIENT_SECRET
});


// zipping the output folder
zipFolder(folder, zipName, function (err) {
  if (err) {
    console.log('oh no!', err);
    process.exit(1);
  } else {
    console.log(`Successfully Zipped ${folder} and saved as ${zipName}`);
    uploadZip(); // on successful zipping, call upload 
  }
});

function uploadZip() {
  // creating file stream to upload
  const extensionSource = fs.createReadStream(`./${zipName}`);
  

  // upload the zip to webstore
  store.uploadExisting(extensionSource).then(res => {
    console.log('Successfully uploaded the ZIP');

    // publish the uploaded zip
    store.publish().then(res => {
      console.log('Successfully published the newer version');
    }).catch((error) => {
      console.log(`Error while publishing uploaded extension: ${error}`);
      process.exit(1);
    });

  }).catch((error) => {
    console.log(`Error while uploading ZIP: ${error}`);
    process.exit(1);
  });
}